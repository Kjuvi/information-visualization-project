FROM python:3.7-alpine3.8
COPY . /code
WORKDIR /code
RUN pip install -r requirements.txt
CMD ["python", "-u", "app.py"]
