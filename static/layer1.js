
function highlightFeature(e) {
  currentCanton = e.target.feature.properties.NAME;
  var layer = e.target;
  layer.setStyle({
    weight: 5,
    dashArray: '',
    fillOpacity: 0.9
  });

  if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
    layer.bringToFront();
  }
  info.update(layer.feature.properties);
}

function resetHighlight(e) {
  geoJson.resetStyle(e.target);
  info.update();
}

function zoomToFeature(e) {
  cantonSelected = e.target.feature.properties.NAME;
  geoJson.resetStyle(e.target);
  map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
  layer.on({
    mouseover: highlightFeature,
    mouseout: resetHighlight,
    click: zoomToFeature
  });
}

$(document).ready(function () {
  loadData("prices");
})

function loadData(type) {
  positionColorCounter = -1;
  resetTable();
  $.get(type, function (response) {
    data = {};
    let responseOrdered = Object.keys(response).sort(function (a, b) { return response[a] - response[b] });
    responseOrdered.reverse().forEach(element => {
      positionColorCounter++;
      var test = {};
      test['position'] = positionColorCounter;
      test['value'] = response[element];
      data[element] = test;
    });

    computeMinAndMax();
    loadMap();
    addContentToTable();
  });
}

function loadMap() {
  loadLayer1();
  loadLayer2();
  addLegendOnMap();
  addInfoOnMap();
  initMapParams();
}

function initMapParams() {
  map.scrollWheelZoom.disable();
  map.on("zoom", function (e) { zoomChanged(e.sourceTarget._zoom) });
}

function loadLayer1() {
  map = L.map('mapid', {
    minZoom: 8,
  }).setView([46.900, 8.212], 8);

  L.tileLayer('', {
    attribution: '&copy; <a href="https://de.yelp.ch/">Yelp</a> data'
  }).addTo(map);


  geoJson = L.geoJson(cantonsData, {
    style: style,
    onEachFeature: onEachFeature
  }).addTo(map);
}

function addInfoOnMap() {
  info = L.control();

  info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
  };

  info.update = function (props) {
    if (!isOnHeatMap) {
      this._div.innerHTML = (props ?
        '<b>' + props.NAME + '</b>' + '<p>(Cliquez pour visualiser les marchés de ce canton)</p>'
        : 'Survolez un canton');
    } else {
      if (cantonSelected) {
        this._div.innerHTML = (props ?
          '<b>' + props.NAME + '</b>' + '<p>(Cliquez pour visualiser les marchés de ce canton)</p>' : '<h4>Selectionner un autre canton pour voir les marchés de celui-ci</h4>'
          + "La liste des marchés analysé sont disponibles en-dessous de la map");
      } else {
        this._div.innerHTML = (props ?
          '<b>' + props.NAME + '</b>' + '<p>(Cliquez pour visualiser les marchés de ce canton)</p>' : '<h4>Selectionner un canton pour voir les marchés de celui-ci</h4>'
          + "La liste des marchés analysé sont disponibles en-dessous de la map");
      }

    }
  };
  info.addTo(map);
}

function addLegendOnMap() {
  if (menu == 1) {
    addLegendHeatMap();
  } else {
    legend = L.control({ position: 'bottomright' });

    legend.onAdd = function (map) {

      var div = L.DomUtil.create('div', 'info legend');
      var grades = ["très cher", "cher", "abordable", "bon marché", "très bon marché"];

      // loop through our density intervals and generate a label with a colored square for each interval
      for (var i = 0; i < grades.length; i++) {
        div.innerHTML += '<i style="background:' + colors[i] + '"></i> ' + grades[i] + '<br>';
      }

      return div;
    };

    legend.addTo(map);
  }
}

function zoomChanged(zoomLevel) {
  if (zoomLevel > 8) {
    if (!isOnHeatMap) {
      addLegendHeatMap();
      isOnHeatMap = true;
      showLayer2();
      hideLayer1();
      loadHeatMap(cantonSelected);
    }
  } else {
    if (isOnHeatMap) {
      isOnHeatMap = false;
      showLayer1();
      hideLayer2();
      cleanMarketTable();
    }
  }
}

function addLegendHeatMap() {
  map.removeControl(legend);
  if (menu === 0) {
    legend = L.control({ position: 'bottomright' });

    legend.onAdd = function (map) {

      var div = L.DomUtil.create('div', 'info legend');
      var grades = ["très cher", "cher", "bon marché", "très bon marché"];

      // loop through our density intervals and generate a label with a colored square for each interval
      div.innerHTML += '<i style="background:' + colors[0] + '"></i> ' + grades[0] + '<br>';
      div.innerHTML += '<i style="background:' + colors[1] + '"></i> ' + grades[1] + '<br>';
      div.innerHTML += '<i style="background:' + colors[3] + '"></i> ' + grades[2] + '<br>';
      div.innerHTML += '<i style="background:' + colors[4] + '"></i> ' + grades[3] + '<br>';

      return div;
    };

    legend.addTo(map);
  } else if (menu === 1) {
    legend = L.control({ position: 'bottomright' });

    legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'info legend');
      var grades = ["très bonne", "bonne", "acceptable", "mauvaise", "très mauvaise"];

      // loop through our density intervals and generate a label with a colored square for each interval
      div.innerHTML += '<b>Appréciation</b><br>';
      div.innerHTML += '<i style="background:' + colors[4] + '"></i> ' + grades[0] + '<br>';
      div.innerHTML += '<i style="background:' + colors[3] + '"></i> ' + grades[1] + '<br>';
      div.innerHTML += '<i style="background:' + colors[2] + '"></i> ' + grades[2] + '<br>';
      div.innerHTML += '<i style="background:' + colors[1] + '"></i> ' + grades[3] + '<br>';
      div.innerHTML += '<i style="background:' + colors[0] + '"></i> ' + grades[4] + '<br>';
      return div;
    };

    legend.addTo(map);
  }
}

function showLayer1() {
  if (groupCircles !== null)
    map.removeLayer(groupCircles);
  geoJson.addTo(map);
}

function hideLayer1() {
  geoJson.removeFrom(map);
}

function style(feature) {
  return {
    weight: 1,
    opacity: 1,
    color: 'white',
    fillOpacity: 0.8,
    fillColor: getColor(feature.properties.NAME)
  };
}

function getColor(d) {
  let position = data[d].position;
  switch (menu) {
    case 0:
      if (position < 5) {
        return colors[0];
      } else if (position < 10) {
        return colors[1];
      } else if (position < 15) {
        return colors[2];
      } else if (position < 20) {
        return colors[3];
      } else {
        return colors[4];
      }
    case 1:
      if (position < 5) {
        return colors[4];
      } else if (position < 10) {
        return colors[3];
      } else if (position < 15) {
        return colors[2];
      } else if (position < 20) {
        return colors[1];
      } else {
        return colors[0];
      }
  }
}

function computeMinAndMax() {
  minValue = Number.MAX_VALUE;
  maxValue = Number.MIN_VALUE;

  Object.keys(data).forEach(key => {
    if (data[key] != null) {
      if (data[key] < minValue)
        minValue = data[key];
      if (data[key] > maxValue)
        maxValue = data[key];
    }
  });
}

function resetTable() {
  $("#bodyTableContent").empty();
}

function addContentToTable() {
  let position = 1;
  for (var key in data) {
    $('#bodyTableContent').append('<tr><td>' + (position > 9 ? position : '0' + position) + '</td><td> ' + key + '</td> </tr>');
    position++
  }
  $('table').tablesort();
}