

function highlightFeatureHeatMap(e) {
  currentCanton = e.target.feature.properties.NAME;
  if (e.target.feature.properties.NAME === currentCantonToHeatMap) {
    return;
  }
  var layer = e.target;

  layer.setStyle({
    weight: 5,
    dashArray: '',
    fillOpacity: 0.9
  });

  if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
    layer.bringToFront();
  }

  info.update(layer.feature.properties);
}

function resetHighlightHeatMap(e) {
  if (e.target.feature.properties.NAME === currentCantonToHeatMap) {
    return;
  }
  geoJson2.resetStyle(e.target);
  info.update();
}

function displayMarkets(e) {
  if (cantonSelected === e.target.feature.properties.NAME)
    return;
    
  cantonSelected = e.target.feature.properties.NAME;
  if (cantonTarget != null) {
    geoJson2.resetStyle(cantonTarget);
  }
  cantonTarget = e.target;
  map.fitBounds(e.target.getBounds());

  loadHeatMap(e.target.feature.properties.NAME);

}

function onEachFeatureHeatMap(feature, layer) {
  layer.on({
    mouseover: highlightFeatureHeatMap,
    mouseout: resetHighlightHeatMap,
    click: displayMarkets
  });
}

function loadLayer2() {
  geoJson2 = L.geoJson(cantonsData, {
    style: styleLayer2,
    onEachFeature: onEachFeatureHeatMap
  });
  groupCircles = L.featureGroup();
}

function loadHeatMap(canton) {
  if (currentCantonToHeatMap === canton) {
    return;
  }
  if (map.getZoom() > 8) {
    $("#loader").show();
    cleanMarketTable();
    map.removeLayer(groupCircles);
    groupCircles = L.featureGroup();

    if (menu === 0) {
      loadPriceMarkets(canton);
    } else if (menu === 1) {
      loadRatingsMarkets(canton);
    }
    currentCantonToHeatMap = canton;
  }

}

function addDataToTable(markets) {
  markets.forEach(market => {
    let rating;
    if (market.rating < 2) {
      rating = '<p style="display:none;">4</p>Très mauvaise';
    } else if (market.rating < 3) {
      rating = '<p style="display:none;">3</p>Mauvaise';
    } else if (market.rating < 4) {
      rating = '<p style="display:none;">2</p>Accéptable';
    } else if (market.rating < 5) {
      rating = '<p style="display:none;">1</p>Bonne';
    } else {
      rating = '<p style="display:none;">0</p>Très bonne';
    }

    let price;

    if (!market.price) {
      price = '<p style="display:none;">4</p>-';
    } else if (market.price.length === 1) {
      price = '<p style="display:none;">1</p>Très bon marché'
    } else if (market.price.length === 2) {
      price = '<p style="display:none;">2</p>Bon marché'
    } else if (market.price.length === 3) {
      price = '<p style="display:none;">3</p>Cher'
    } else {
      price = '<p style="display:none;">4</p>Très Cher'
    }

    dataTable.row.add([market.name, market.coordinates.latitude + ' / ' + market.coordinates.longitude, price, rating]).draw(false);
  });
}

async function loadRatingsMarkets(canton) {
  let veryGood = [];
  let good = [];
  let normal = [];
  let bad = [];
  let veryBad = [];

  $.get("canton/" + canton, function (response) {
    response.forEach(function (e) {
      addDataToTable(e.data.search.business);
      for (let i = 0; i < e.data.search.business.length; i++) {
        let rating = e.data.search.business[i].rating;
        if (rating !== null) {
          switch (e.data.search.business[i].rating) {
            case 1:
              veryBad.push(e.data.search.business[i]);
              break;
            case 2:
              bad.push(e.data.search.business[i]);
              break;
            case 3:
              normal.push(e.data.search.business[i]);
              break;
            case 4:
              good.push(e.data.search.business[i]);
              break;
            default:
              veryGood.push(e.data.search.business[i]);
          }
        }
      }
    });
    for (let i = 0; i < veryBad.length; i++) {
      let commerce = veryBad[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 0);
    }
    for (let i = 0; i < bad.length; i++) {
      let commerce = bad[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 1);
    }
    for (let i = 0; i < normal.length; i++) {
      let commerce = normal[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 2);
    }
    for (let i = 0; i < good.length; i++) {
      let commerce = good[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 3);
    }
    for (let i = 0; i < veryGood.length; i++) {
      let commerce = veryGood[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 4);
    }
    map.addLayer(groupCircles);
    $("#loader").hide();
  });
}

async function loadPriceMarkets(canton) {
  let veryExpensive = [];
  let expensive = [];
  let cheap = [];
  let veryCheap = [];

  $.get("canton/" + canton, function (response) {
    response.forEach(function (e) {
      addDataToTable(e.data.search.business);
      for (let i = 0; i < e.data.search.business.length; i++) {
        let price = e.data.search.business[i].price;
        if (price !== null) {
          let priceLength = e.data.search.business[i].price.length;
          switch (e.data.search.business[i].price.length) {
            case 1:
              veryCheap.push(e.data.search.business[i]);
              break;
            case 2:
              cheap.push(e.data.search.business[i]);
              break;
            case 3:
              expensive.push(e.data.search.business[i]);
              break;
            default:
              veryExpensive.push(e.data.search.business[i]);
          }
        }
      }
    });

    for (let i = 0; i < veryCheap.length; i++) {
      let commerce = veryCheap[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 1);
    }
    for (let i = 0; i < cheap.length; i++) {
      let commerce = cheap[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 2);
    }
    for (let i = 0; i < expensive.length; i++) {
      let commerce = expensive[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 3);
    }
    for (let i = 0; i < veryExpensive.length; i++) {
      let commerce = veryExpensive[i];
      addCircles(commerce.name, commerce.coordinates.latitude, commerce.coordinates.longitude, 4);
    }
    map.addLayer(groupCircles);
    $("#loader").hide();
  });
}



function addCircles(commerce, posX, posY, rating) {
  if (menu === 0) {
    var circle = L.circle([posX, posY], { //46.9546,7.3949
      stroke: false,
      fillColor: colors[4 - rating],
      fillOpacity: 1,
      radius: 1000
    }).addTo(groupCircles);
  } else if (menu === 1) {
    var circle = L.circle([posX, posY], { //46.9546,7.3949
      stroke: false,
      fillColor: colors[rating],
      fillOpacity: 1,
      radius: 1000
    }).addTo(groupCircles);
  }

}

function styleLayer2(feature) {
  return {
    weight: 2,
    opacity: 1,
    color: 'white',
    fillOpacity: 0.6
  };
}

function showLayer2() {
  geoJson2.addTo(map);
  for (var target in map._targets) {
    if (map._targets[target].hasOwnProperty("feature") && map._targets[target].feature.properties.NAME === cantonSelected) {
      cantonTarget = map._targets[target];
      colorCantonSelected();
    }
  }
  info.update();
}

function hideLayer2() {
  if (cantonTarget != null)
    geoJson2.resetStyle(cantonTarget);
  geoJson2.removeFrom(map);
}

function colorCantonSelected() {
  cantonTarget.setStyle({
    color: "white",
    weight: 5,
    dashArray: '',
    fillOpacity: 0.9
  });
}

function uncolorCantonSelected() {
  cantonTarget.setStyle({
    weight: 2,
    opacity: 1,
    color: 'white',
    fillOpacity: 0.6
  });
}

function cleanMarketTable() {
  //let table = $('#markets').DataTable();
  dataTable.clear();
  dataTable.draw();
  //$("#marketTable").empty();
}