Description of the Zip content. 
2018_01_sBOUND_InfoD.pdf : Description of the dataset swissBOUNDARIES3D in German
2018_01_sBOUND_InfoF.pdf : Description of the dataset swissBOUNDARIES3D in French
Licence.txt : Terms of use in German, French and English
swissBOUNDARIES3D_LV03.zip : Original Data in local Swiss reference frames LV03 in different formats
swissBOUNDARIES3D_LV95.zip : Original Data in local Swiss reference frames LV95 in different formats


State of data: 1.1.2018

Bern, 24.01.2018