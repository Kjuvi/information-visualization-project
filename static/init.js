var menu = 0;
var geoJson; //layer 1
var geoJson2; //layer 2
var map;
var data;
var minValue;
var maxValue;
var positionColorCounter = -1;
var colors = ['#E60000', '#E66000', '#E6BF00', '#ADE600', '#4DE600'];
var info;
var currentCanton = "";
var currentCantonToHeatMap = "";
var cantonSelected = "";
var cantonTarget;
var controlLayers;
var isOnHeatMap = false;
var listMarket = [];
var groupCircles;
var dataTable;
var legend;

function showSplashScreen() {
  var showSplashScreen = getCookie("showSplashScreen");
  if (showSplashScreen !== "false") {
    $("#splashscreen").show();
  }
}

function closeSplashScreen() {
  if ($("#dontShowCheckBox").is(':checked')) {
    document.cookie = "showSplashScreen=false; expires=Thu, 18 Dec 2019 12:00:00 UTC";
  }
  //$("#splashscreen").slideUp('slow');
  $("#splashscreen").fadeTo(500, 0, function () {
    $("#splashscreen").hide();
  });
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

$(document).ready(function () {
  $("#loader").hide();
  dataTable = $('#markets').DataTable({
    "language": {
      "lengthMenu": "Afficher _MENU_  marchés par page",
      "zeroRecords": "Aucun marché trouvé...",
      "info": "",
      "search" : "Recherche",
      "infoEmpty": "Veuillez sélectionner un canton",
      "infoFiltered": "(filtered from _MAX_ total records)",
      "paginate":{
        "next" : ">",
        "previous" : "<",
        "first" : "<<",
        "last" : ">>"
      }
    }
  });
  showSplashScreen();
  initMenu();
})

function initMenu() {
  $("#menuBtn").click(function () {
    $('.ui.sidebar')
      .sidebar('toggle');
  });
}


function loadMenuOption1() {
  resetAll();
  menu = 0;
  $("#title").html('<i class="ui map marker alternate icon"></i>Les cantons les plus chèrs de Suisse');
  $('.ui.sidebar').sidebar('toggle');
  loadData("prices");
  
}

function loadMenuOption2() {
  resetAll();
  menu = 1;
  $("#title").html('<i class="ui map marker alternate icon"></i>Les cantons les plus appréciés de Suisse');
  $('.ui.sidebar').sidebar('toggle');
  loadData("ratings"); 
}

function resetAll() {
  isOnHeatMap = false;
  map.remove();
  cleanMarketTable();
  resetTable();
  if (groupCircles !== null)
    map.removeLayer(groupCircles);
}