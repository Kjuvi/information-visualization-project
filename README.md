# Information Visualization Project

The **Swiss Business Comparator** visualization application is a project for
the HES-SO Master course _Information visualization_.

It presents an interactive map of Switzerland in which the user can compare
various cities's businesses according to different search criteria like:

- The mean rating of the cities;
- The mean prices of the cities;
- How's the businesses night life;
- YouNameIt...

It targets obviously the Swiss businesses users (i.e. the Swiss population and
all the people who are travelling in Switzerland), but it is not only narrowed
to them. Indeed, leaving the semantics apart, this application is a good example
of how the InfoViz technology can highlight some "hidden" information which are contained in a raw data set.

## Requirements

To run the back-end, the following dependencies are required:

```bash
1. docker
2. docker-compose
```

## Getting Started

The first thing to do is to copy the `.env.example` file to `.env` and add your Yelp API key to this `.env` file.

Then, in order to run the server, the docker daemon has to be running, so let's make
sure that is the case:

```bash
$ sudo systemctl start docker
```

Finally, simply start the containers:

```bash
$ sudo docker-compose up
```

To bring everything down:

```bash
$ sudo docker-compose down --volumes
```

Opening a browser to 0.0.0.0:5000 allow to access the front end.

## A word on the source of the data

The data used for the visualization come from the
[Yelp API](https://www.yelp.com/developers). The reason that Yelp has been
chosen as the source can be summarized as follows:

- Yelp is one of the most complete "database" regarding the businesses
  information in the world.
- The Yelp API can be accessed by [GraphQL](https://graphql.org/), which allow
  to customize the query in order to retrieved only what's necessary.
- There are only little limitations regarding the API access.

## Presentation
https://slides.com/andrenetodasilva/sbc/

