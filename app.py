import os
import json
import ast
import redis

from flask import Flask, send_from_directory, jsonify
from backend.data_preparation import create_dataset_if_not_exist, insert_cantons_into_db, insert_businesses_into_db

app = Flask(__name__)
db = redis.Redis(host='redis', port=6379)


@app.route("/")
def hello():
    return send_from_directory('./', 'index.html')


@app.route("/prices")
def get_prices():
    prices = db.get('prices').decode('UTF-8')
    prices_json = ast.literal_eval(prices)
    return jsonify(prices_json)


@app.route("/ratings")
def get_ratings():
    ratings = db.get('ratings').decode('UTF-8')
    ratings_json = ast.literal_eval(ratings)
    return jsonify(ratings_json)


@app.route("/canton/<fase>")
def get_canton_ratings(fase):
    canton_info = db.get(fase).decode('UTF-8')
    canton_info_json = ast.literal_eval(canton_info)
    return jsonify(canton_info_json)


if __name__ == "__main__":
    create_dataset_if_not_exist(db)
    app.run(host="0.0.0.0", port=5000, debug=True)
