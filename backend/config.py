import os

API_KEY = os.environ["YELP_API_KEY"]

HEADERS = {"content-type": "application/json", "Authorization": f"Bearer {API_KEY}"}

CITIES = [
    'Aarau', 'Herisau', 'Appenzell', 'Liestal', 'Basel', 'Bern', 'Fribourg',
    'Geneva', 'Glarus', 'Chur', 'Delémont', 'Lucerne', 'Neuchâtel', 'Stans',
    'Sarnen', 'Schaffhausen', 'Schwyz', 'Solothourn', 'Saint-Gallen',
    'Frauenfeld', 'Bellinzona', 'Altdorf', 'Sion', 'Lausanne', 'Zug', 'Zürich'
]

CANTONS = [
    'Aargau', 'Appenzell Ausserrhoden', 'Appenzell Innerrhoden', 'Basel-Landschaft', 'Basel-Stadt', 'Bern', 'Fribourg',
    'Genève', 'Glarus', 'Graubünden', 'Jura', 'Luzern', 'Neuchâtel', 'Nidwalden',
    'Obwalden', 'Schaffhausen', 'Schwyz', 'Solothurn', 'St. Gallen',
    'Thurgau', 'Ticino', 'Uri', 'Valais', 'Vaud', 'Zug', 'Zürich'
]

STATES = [
    'AG', 'AR', 'AI', 'BL', 'BS', 'BE', 'FR', 
    'GE', 'GL', 'GR', 'JU', 'LU', 'NE', 'NW', 
    'OW', 'SH', 'SZ', 'SO', 'SG',
    'TG', 'TI', 'UR', 'VS',  'VD', 'ZG', 'ZH'
]

URL = "https://api.yelp.com/v3/graphql"