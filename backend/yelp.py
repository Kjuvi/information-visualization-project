import json
import os
import requests

from backend.config import API_KEY, HEADERS, CITIES, CANTONS, STATES, URL


def get_number_of_business(city, state, radius):
    """
    Return the number of business that Yelp has found 
    according to the given location .
    """
    payload = {
        'query': f'''
        {{
            search(
                location: "{city} {state}",
                radius: {radius}
            ) 
            {{
                total
            }}
        }}
        '''
    }

    response = (requests.post(url=URL, json=payload, headers=HEADERS)).json()
    number_of_business = response['data']['search']['total']

    max_offset = 980
    return number_of_business if number_of_business < max_offset else max_offset


def get_payload(city, state, i):
    return {
            'query': f'''
            {{
                search ( 
                    location:"{city} {state}",
                    radius: 40000,
                    offset: {i}
                )
                {{
                    business {{
                        name,
                        rating,
                        price,
                        coordinates {{
                            latitude,
                            longitude
                        }},
                        location {{
                            state
                        }}
                    }}
                }}
            }}'''
        }


def query_yelp(city, state, number_of_business):
    offset = 20 # Yelp return 20 result per query
    response = []

    for i in range(0, number_of_business, offset):
        payload = get_payload(city, state, i)
        response.append((requests.post(url=URL, json=payload, headers=HEADERS)).json())
    
    return response


def retrieve():
    radius = 40000
    path = 'dataset/'

    current = 0
    total = len(CITIES)

    for i, city in enumerate(CITIES):
        state = STATES[i]
        number_of_business = get_number_of_business(city, state, radius)
        response = query_yelp(city, state, number_of_business)

        with open(f'{path}{CANTONS[i]}.json', 'w') as f:
            f.write(json.dumps(response))

        progress = round((current / total) * 100, 2)
        print(f'Progress: {progress}%')
        current += 1