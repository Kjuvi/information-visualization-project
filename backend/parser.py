import json
import os
from backend.config import CITIES, CANTONS, STATES


def parse_dataset():
    files_list = [
        f'dataset/{canton}.json'
        for canton in CANTONS
    ]

    prices = {}
    ratings = {}

    for i, filename in enumerate(files_list):
        canton = CANTONS[i]
        mean_rating, mean_price = parse_prices_and_ratings(filename, i)
        ratings[canton] = mean_rating
        prices[canton] = mean_price
    
    return ratings, prices


def parse_prices_and_ratings(filename, i):
    with open(filename) as canton:
        dataset = json.load(canton)
        dataset = remove_data_outside_of_canton(dataset, STATES[i])
        
        mean_rating = 0
        rating_number = 0
        mean_price = 0
        price_number = 0
        
        for data in dataset:
            for business in data["data"]["search"]["business"]:
                if "rating" in business.keys() and business['rating'] is not None:
                    mean_rating += business["rating"]
                    rating_number += 1
                if "price" in business.keys() and business['price'] is not None:
                    mean_price += len(business["price"])
                    price_number += 1 


        mean_rating  = (
            round(mean_rating / rating_number, 2) 
            if rating_number > 0 else -1
        )
        mean_price = (
            round(mean_price / price_number, 2)
            if price_number > 0 else -1
        )
        
        return mean_rating, mean_price
    

def remove_data_outside_of_canton(dataset, state):
    cleaned_dataset = []
     
    for data in dataset:
        cleaned_dataset.append({"data": {"search": {"business": []}}})
        for business in data["data"]["search"]["business"]:
            if business['location']['state'] == state:
                cleaned_dataset[-1]["data"]["search"]["business"].append(business)

    return cleaned_dataset